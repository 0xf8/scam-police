use crate::{
    keywords::{KeywordCategory, Keywords},
    CONFIG,
};
use matrix_sdk::{
    room::Joined,
    ruma::events::room::message::{OriginalRoomMessageEvent, RoomMessageEventContent},
};
use serde_json::json;

#[derive(Debug)]
pub enum JudgementResult {
    Ok,
    MaybeScam,  // hit atleast one category
    LikelyScam, // hit all categories
}

impl JudgementResult {
    pub fn to_json_var(&self) -> &str {
        match self {
            Self::Ok => "Ok",
            Self::MaybeScam => "MaybeScam",
            Self::LikelyScam => "LikelyScam",
        }
    }
}

pub struct Judgement {
    pub text: String,
}

impl Judgement {
    pub fn judge(&self) -> anyhow::Result<JudgementResult> {
        // Load keywords
        let mut keywords = CONFIG.keywords.clone();
        let keywords = keywords
            .as_object_mut()
            .unwrap()
            .get_mut("keywords")
            .unwrap();

        // Turn json into Keywords
        let verbs = Keywords::create("verbs", &keywords["verbs"]);
        let currencies = Keywords::create("currencies", &keywords["currencies"]);
        let socials = Keywords::create("socials", &keywords["socials"]);

        // Count occurences
        let mut counter = KeywordCategory::create_counter_map();
        counter.insert(KeywordCategory::Verb, verbs.find(&self.text));
        counter.insert(KeywordCategory::Currency, currencies.find(&self.text));
        counter.insert(KeywordCategory::Social, socials.find(&self.text));

        let mut count_all = 0;
        let total = counter.len();
        for (_category, count) in counter.to_owned() {
            if count > 0 {
                count_all = count_all + 1;
            }
        }

        if count_all == 0 {
            return Ok(JudgementResult::Ok);
        };
        if count_all < total {
            return Ok(JudgementResult::MaybeScam);
        };
        Ok(JudgementResult::LikelyScam)
    }

    pub async fn send_debug(&self, room: &Joined) -> anyhow::Result<()> {
        // Load keywords
        let mut keywords = CONFIG.keywords.clone();
        let keywords = keywords
            .as_object_mut()
            .unwrap()
            .get_mut("keywords")
            .unwrap();

        // Turn json into Keywords
        let verbs = Keywords::create("verbs", &keywords["verbs"]);
        let currencies = Keywords::create("currencies", &keywords["currencies"]);
        let socials = Keywords::create("socials", &keywords["socials"]);

        // Count occurences
        let mut counter = KeywordCategory::create_counter_map();
        counter.insert(KeywordCategory::Verb, verbs.find(&self.text));
        counter.insert(KeywordCategory::Currency, currencies.find(&self.text));
        counter.insert(KeywordCategory::Social, socials.find(&self.text));

        let mut count_all = 0;
        let total = counter.len();
        for (_category, count) in counter.to_owned() {
            if count > 0 {
                count_all = count_all + 1;
            }
        }

        let mut result = JudgementResult::LikelyScam;
        if count_all == 0 {
            result = JudgementResult::Ok
        }
        if count_all < total {
            result = JudgementResult::MaybeScam
        }

        // Send message
        let msg = RoomMessageEventContent::text_html(
            format!("{counter:?}, {count_all}/{total}, {result:?}"),
            format!("<code>{counter:?}</code><br>Categories covered: <code>{count_all}/{total}</code><br>Verdict: <code>{result:?}</code>"));
        room.send(msg, None).await.expect("Couldn't send message");

        Ok(())
    }

    pub async fn alert(
        room: &Joined,
        event: &OriginalRoomMessageEvent,
        result: JudgementResult,
        is_reply: bool,
    ) -> anyhow::Result<()> {
        let mut responses = CONFIG.responses.clone();
        let responses = responses.as_object_mut().unwrap();

        // Determine which message to send
        let section = if is_reply {
            responses["reply"].as_object().unwrap()
        } else {
            responses["message"].as_object().unwrap()
        };

        let response_type = section.get(result.to_json_var()).unwrap();
        if response_type.is_null() {
            anyhow::bail!("Called alert with result that has no detection message");
        }

        let response_type = response_type.as_object().unwrap();
        let plain = response_type["plain"].as_str().unwrap();
        let html = response_type["html"].as_str().unwrap();

        // Send message
        let msg = RoomMessageEventContent::text_html(plain, html);
        room.send(msg, None).await.expect("Couldn't send message");
        // Todo: Add room config?
//        let reply = msg.make_reply_to(event);
//        room.send(reply, None).await.expect("Couldn't send message");

        // Send reaction
        if !is_reply {
            room.send_raw(
                json!({
                "m.relates_to": {
                    "rel_type": "m.annotation",
                    "event_id": event.event_id.to_string(),
                    "key": "🚨🚨 SCAM 🚨🚨"
                }}),
                "m.reaction",
                None,
            )
            .await
            .expect("Couldn't send reaction");
        }

        Ok(())
    }
}
