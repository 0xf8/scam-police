use matrix_sdk::{
    config::SyncSettings, ruma::api::client::filter::FilterDefinition, Client, Session,
};
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use reqwest::Client as http;
use rpassword::prompt_password;
use serde::{Deserialize, Serialize};
use serde_json::{from_str, Value};
use std::path::{Path, PathBuf};
use tokio::fs;

#[derive(Debug, Serialize, Deserialize)]
pub struct ClientSession {
    homeserver: String,
    db_path: PathBuf,
    passphrase: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FullSession {
    client_session: ClientSession,
    user_session: Session,
    #[serde(skip_serializing_if = "Option::is_none")]
    sync_token: Option<String>,
}

//
// Matrix Login & Init
//

pub async fn login(data_dir: &Path, session_file: &Path, mxid: String) -> anyhow::Result<Client> {
    println!("[*] No previous session, logging in with mxid...");

    let (user, hs) = resolve_mxid(mxid).await?;
    let (client, client_session) = build_client(data_dir, hs).await?;

    loop {
        let password = prompt_password("Password\n> ")?;

        match client
            .login_username(&user, &password)
            .initial_device_display_name("scam-police")
            .send()
            .await
        {
            Ok(_) => {
                println!("[*] Logged in as {user}");
                break;
            }
            Err(error) => {
                println!("[!] Error logging in: {error}");
                println!("[!] Please try again\n");
            }
        }
    }

    let user_session = client
        .session()
        .expect("A logged-in client should have a session");
    let serialized_session = serde_json::to_string(&FullSession {
        client_session,
        user_session,
        sync_token: None,
    })?;
    fs::write(session_file, serialized_session).await?;
    Ok(client)
}

pub async fn build_client(data_dir: &Path, hs: String) -> anyhow::Result<(Client, ClientSession)> {
    let mut rng = thread_rng();

    let db_subfolder: String = (&mut rng)
        .sample_iter(Alphanumeric)
        .take(7)
        .map(char::from)
        .collect();
    let db_path = data_dir.join(db_subfolder);

    let passphrase: String = (&mut rng)
        .sample_iter(Alphanumeric)
        .take(32)
        .map(char::from)
        .collect();

    match Client::builder()
        .homeserver_url(&hs)
        .sled_store(&db_path, Some(&passphrase))?
        .build()
        .await
    {
        Ok(client) => {
            println!("[*] Homeserver OK");
            return Ok((
                client,
                ClientSession {
                    homeserver: hs,
                    db_path,
                    passphrase,
                },
            ));
        }
        Err(error) => match &error {
            matrix_sdk::ClientBuildError::AutoDiscovery(_)
            | matrix_sdk::ClientBuildError::Url(_)
            | matrix_sdk::ClientBuildError::Http(_) => {
                anyhow::bail!("[!] {error:?}");
            }
            _ => {
                return Err(error.into());
            }
        },
    }
}

//
// Helper Functions
//

// Resolve mxid into user and hs
pub async fn resolve_mxid(mxid: String) -> anyhow::Result<(String, String)> {
    if mxid.get(0..1).unwrap() != "@" || !mxid.contains(":") {
        anyhow::bail!("Invalid mxid");
    }

    let sep = mxid.find(":").unwrap();
    let user = mxid.get(1..sep).unwrap().to_string();
    let hs = resolve_homeserver(mxid.get((sep + 1)..).unwrap().to_string()).await?;

    Ok((user, hs))
}

// Resolve homeserver
pub async fn resolve_homeserver(homeserver: String) -> anyhow::Result<String> {
    let mut hs = homeserver;
    if !hs.contains("://") {
        hs = format!("https://{hs}");
    }

    if hs.chars().last().unwrap().to_string() == "/" {
        hs.pop();
    }

    let ident = http::new()
        .get(format!("{hs}/.well-known/matrix/client"))
        .send()
        .await;
    match ident {
        Ok(r) => {
            let body = r.text().await?;
            let json: Value = from_str(&body)?;

            let discovered = json["m.homeserver"]["base_url"].as_str().unwrap();

            Ok(discovered.to_string())
        }
        Err(e) => Err(e.into()),
    }
}

//
// Persistence
//

pub async fn sync<'a>(
    client: Client,
    initial_sync_token: Option<String>,
) -> anyhow::Result<(Client, SyncSettings<'a>)> {
    println!("[*] Initial sync...");

    let filter = FilterDefinition::empty();

    let mut sync_settings = SyncSettings::default().filter(filter.into());

    if let Some(sync_token) = initial_sync_token {
        sync_settings = sync_settings.token(sync_token);
    }

    loop {
        match client.sync_once(sync_settings.clone()).await {
            Ok(response) => {
                sync_settings = sync_settings.token(response.next_batch.clone());
                persist_sync_token(response.next_batch).await?;
                break;
            }
            Err(error) => {
                println!("[!] An error occurred during initial sync: {error}");
                if error.to_string().contains("[401 / M_UNKNOWN_TOKEN]") {
                    anyhow::bail!("Unknown token. You need to login again");
                }
                println!("[!] Trying again…");
            }
        }
    }

    println!("[*] The bot is ready!");

    Ok((client, sync_settings))
}

pub async fn persist_sync_token(sync_token: String) -> anyhow::Result<()> {
    let data_dir = dirs::data_dir()
        .expect("no data_dir directory found")
        .join("scam_police");
    let session_file = data_dir.join("session");

    let serialized_session = fs::read_to_string(&session_file).await?;
    let mut full_session: FullSession = from_str(&serialized_session)?;

    full_session.sync_token = Some(sync_token);
    let serialized_session = serde_json::to_string(&full_session)?;
    fs::write(session_file, serialized_session).await?;

    Ok(())
}

pub async fn restore_session(session_file: &Path) -> anyhow::Result<(Client, Option<String>)> {
    let serialized_session = fs::read_to_string(session_file).await?;
    let FullSession {
        client_session,
        user_session,
        sync_token,
    } = from_str(&serialized_session)?;

    let client = Client::builder()
        .homeserver_url(client_session.homeserver)
        .sled_store(client_session.db_path, Some(&client_session.passphrase))?
        .build()
        .await?;

    println!("[*] Restoring session for {}…", user_session.user_id);

    client.restore_login(user_session).await?;

    Ok((client, sync_token))
}
