use serde_json::Value;

pub struct Config {
    pub keywords: Value,
    pub responses: Value,
}

impl Config {
    pub fn load() -> Config {
        let keywords_reader =
            std::fs::File::open("config/keywords.json").expect("Couldn't find keywords.json");
        let keywords: Value =
            serde_json::from_reader(keywords_reader).expect("Couldn't read keywords");

        let responses_reader =
            std::fs::File::open("config/responses.json").expect("Couldn't find responses.json");
        let responses: Value =
            serde_json::from_reader(responses_reader).expect("Couldn't read responses");

        Self {
            keywords,
            responses,
        }
    }
}
