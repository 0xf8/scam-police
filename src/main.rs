use matrix_sdk::{
    room::Room,
    ruma::{
        events::room::message::{
            MessageType, OriginalSyncRoomMessageEvent, Relation, RoomMessageEvent,
        },
        OwnedRoomId,
    },
    Error, LoopCtrl,
};
use once_cell::sync::Lazy;

pub mod config;
pub mod judge;
pub mod keywords;
pub mod matrix;

static CONFIG: Lazy<config::Config> = Lazy::new(|| config::Config::load());

async fn on_room_message(event: OriginalSyncRoomMessageEvent, room: Room) -> anyhow::Result<()> {
    if let Room::Joined(room) = room {
        let orig_event = event
            .to_owned()
            .into_full_event(OwnedRoomId::from(room.room_id()));

        // Ignore non-text
        let MessageType::Text(text_content) = event.to_owned().content.msgtype else {
            return Ok(());
        };

        // Too short to be a scam lol
        if text_content.body.chars().count() < 12 {
            return Ok(());
        }

        let text_content = text_content.body.to_lowercase();
        let debug = text_content.contains(";spdebug");

        // Ignore own messages
        if !debug
            && event.sender
                == room
                    .client()
                    .user_id()
                    .expect("Couldn't get user_id")
                    .to_string()
        {
            return Ok(());
        }

        let judgement = judge::Judgement { text: text_content };

        // Handle replies
        if let Some(relation) = orig_event.to_owned().content.relates_to {
            if let Some(event) = match relation {
                Relation::Reply { in_reply_to } => {
                    let replied_event = room.event(&in_reply_to.event_id).await?;
                    let t = replied_event.event.get_field::<&str>("type")?;

                    if t.as_deref() == Some("m.room.message") {
                        let event = replied_event.event.deserialize_as::<RoomMessageEvent>()?;
                        Some(event.to_owned())
                    } else {
                        None
                    }
                }
                _ => None,
            } {
                let event = event.as_original().unwrap();
                let content = event.content.to_owned().body().to_lowercase();

                if !debug
                    && event.sender
                        == room
                            .client()
                            .user_id()
                            .expect("Couldn't get user_id")
                            .to_string()
                {
                    return Ok(());
                }

                let reply_judgement = judge::Judgement { text: content };

                if debug {
                    reply_judgement.send_debug(&room).await?;
                    return Ok(());
                }

                match reply_judgement.judge()? {
                    judge::JudgementResult::Ok => (),
                    judge::JudgementResult::MaybeScam => (),
                    judge::JudgementResult::LikelyScam => {
                        judge::Judgement::alert(
                            &room,
                            &orig_event,
                            judge::JudgementResult::LikelyScam,
                            true,
                        )
                        .await?;
                        return Ok(());
                    }
                }
            }
        }

        match judgement.judge()? {
            judge::JudgementResult::Ok => return Ok(()),
            judge::JudgementResult::MaybeScam => return Ok(()),
            judge::JudgementResult::LikelyScam => {
                judge::Judgement::alert(
                    &room,
                    &orig_event,
                    judge::JudgementResult::LikelyScam,
                    false,
                )
                .await?;
                return Ok(());
            }
        }
    }

    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let args: Vec<String> = std::env::args().collect();

    let data_dir = dirs::data_dir()
        .expect("no data_dir directory found")
        .join("scam_police");
    let session_file = data_dir.join("session");

    let (client, sync_token) = if session_file.exists() {
        matrix::restore_session(&session_file).await?
    } else if args.len() > 1 {
        (
            matrix::login(&data_dir, &session_file, args.get(1).unwrap().to_owned()).await?,
            None,
        )
    } else {
        anyhow::bail!(
            "No previous session found, please run \"{} <MXID>\"",
            args.get(0).unwrap()
        );
    };

    let (client, sync_settings) = match matrix::sync(client, sync_token).await {
        Ok(c) => c,
        Err(e) => {
            std::fs::remove_dir_all(data_dir)?;
            anyhow::bail!("{e}");
        }
    };

    client.add_event_handler(on_room_message);

    client
        .sync_with_result_callback(sync_settings, |sync_result| async move {
            let response = sync_result?;

            matrix::persist_sync_token(response.next_batch)
                .await
                .map_err(|err| Error::UnknownError(err.into()))?;

            Ok(LoopCtrl::Continue)
        })
        .await?;

    Ok(())
}
